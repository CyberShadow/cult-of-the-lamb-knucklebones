Cult of the Lamb - Knucklebones solver
======================================

Solver for the "Knucklebones" dice / board minigame in [Cult of the Lamb](https://store.steampowered.com/app/1313140).

Public instance: https://cy.md/ta/cult-of-the-lamb/knucklebones/

Discussion: https://steamcommunity.com/app/1313140/discussions/0/5267542371389134136/
