import std.algorithm.comparison;
import std.algorithm.iteration;
import std.exception;
import std.file;
import std.stdio;
import std.string;

import ae.sys.datamm;
import ae.utils.meta;

import ai;
import common;
import graph;
import packing;

struct Problem
{
	Game initialGame;
	Die rolledDie;
}
Problem parseProblem(string s)
{
	auto lines = s.splitLines();
	enforce(lines.length == 9);
	enforce(lines.map!(line => line.length).equal([3, 3, 3, 0, 3, 3, 3, 0, 1]));
	Problem problem;
	Die toDie(char c)
	{
		switch (c)
		{
			case '1': return 1;
			case '2': return 2;
			case '3': return 3;
			case '4': return 4;
			case '5': return 5;
			case '6': return 6;
			case '.': return 0;
			default: throw new Exception("Bad die char: " ~ c);
		}
	}
	problem.initialGame.board[Player.other  ][0][2] = toDie(lines[0][0]);
	problem.initialGame.board[Player.other  ][1][2] = toDie(lines[0][1]);
	problem.initialGame.board[Player.other  ][2][2] = toDie(lines[0][2]);
	problem.initialGame.board[Player.other  ][0][1] = toDie(lines[1][0]);
	problem.initialGame.board[Player.other  ][1][1] = toDie(lines[1][1]);
	problem.initialGame.board[Player.other  ][2][1] = toDie(lines[1][2]);
	problem.initialGame.board[Player.other  ][0][0] = toDie(lines[2][0]);
	problem.initialGame.board[Player.other  ][1][0] = toDie(lines[2][1]);
	problem.initialGame.board[Player.other  ][2][0] = toDie(lines[2][2]);

	problem.initialGame.board[Player.current][0][0] = toDie(lines[4][0]);
	problem.initialGame.board[Player.current][1][0] = toDie(lines[4][1]);
	problem.initialGame.board[Player.current][2][0] = toDie(lines[4][2]);
	problem.initialGame.board[Player.current][0][1] = toDie(lines[5][0]);
	problem.initialGame.board[Player.current][1][1] = toDie(lines[5][1]);
	problem.initialGame.board[Player.current][2][1] = toDie(lines[5][2]);
	problem.initialGame.board[Player.current][0][2] = toDie(lines[6][0]);
	problem.initialGame.board[Player.current][1][2] = toDie(lines[6][1]);
	problem.initialGame.board[Player.current][2][2] = toDie(lines[6][2]);

	problem.rolledDie = toDie(lines[8][0]);

	return problem;
}

void explore(Game game, const ScoreTable scores, int maxDepth, int depth = 0)
{
	writefln("%*s %s -> %s -> %s: %s", depth * 4, "", game, game.packGame, game.packGame.unpackGame, scores[game.packGame]);
	if (depth < maxDepth)
	{
		Score scoreSum = 0;
		foreach (Die die; dieMin .. dieMax + 1)
		{
			writefln("%*s - Roll %d:", depth * 4, "", die);

			Score bestScore = minScore;
			foreach (Move move; 0 .. numMoves)
				if (game.canPlace(move))
				{
					writefln("%*s   - Move to %d:", depth * 4, "", move);
					auto game2 = game.place(move, die).flipBoard;
					explore(game2, scores, maxDepth, depth + 1);
					Score moveScore = -scores[game2.packGame];
					if (moveScore > bestScore)
						bestScore = moveScore;
				}
			writefln("%*s   Best score: %s", depth * 4, "", bestScore);
			if (bestScore == minScore)
				assert(false, "No moves available?");
			scoreSum += bestScore;
		}
		auto avgScore = scoreSum / 6;
		writefln("%*s   Avg. score: %s", depth * 4, "", avgScore);
	}
}

void main()
{
	enum opponent = Opponent.ai1;
	if (!scoreFileName(opponent).exists)
		generateScoreFile!opponent();

	auto scoreData = mapFile(scoreFileName(opponent), MmMode.read);
	auto scores = cast(const(Score)[])scoreData.contents;

	auto problem = "game.txt".readText.parseProblem;
	// writefln("Score before move: %s (%s)", scores[problem.initialGame.packGame], problem.initialGame.getPlayerScores);
	auto analysis = analyzeGame(problem.initialGame, problem.rolledDie, scores, opponent);
	// explore(problem.initialGame/*.place(problem.rolledDie, analysis.bestMoves[0]).flipBoard*/, scores, 2);
	writefln("Best move: %-(%s / %) (chances of winning: %-(%f%%%| | %))",
		analysis.bestMoves.map!(move => ["left column", "middle column", "right column"][move]),
		analysis.scores[].map!(score => score * 50 + 50));
}
