import std.file;
import std.random;
import std.stdio;

import ae.sys.data;
import ae.sys.datamm;
import ae.utils.meta;

import ai;
import common;
import graph;
import packing;

Game randomGame()
{
	// foreach (player; 0 .. numPlayers)
	// 	foreach (column; 0 .. numColumns)
	// 		foreach (row; 0 .. uniform(0, numRows+1))
	// 			game.board[player][column][row] = uniform!"[]"(dieMin, dieMax);

	Game game;
	foreach (n; 0 .. uniform(0, 30))
	{
		Move move = uniform(Move(0), Move(numColumns));
		if (!game.canPlace(move))
			return game;
		game = game.place(move, uniform!"[]"(dieMin, dieMax));
		game = game.flipBoard();
	}
	return game;
}

void main()
{
	Data[enumLength!Opponent] scoreData;
	ScoreTable[enumLength!Opponent] scores;
	foreach (opponent; Opponent.init .. enumLength!Opponent)
		scoreData[opponent] = mapFile(scoreFileName(opponent), MmMode.read);
	foreach (opponent; Opponent.init .. enumLength!Opponent)
		scores[opponent] = cast(Score[])scoreData[opponent].contents;

	while (true)
	{
		auto opponent1 = uniform(Opponent.init, enumLength!Opponent);
		auto opponent2 = uniform(Opponent.init, enumLength!Opponent);
		if (opponent1 == opponent2)
			continue;
		auto game = randomGame();
		Die die = uniform!"[]"(dieMin, dieMax);
		auto analysis1 = analyzeGame(game, die, scores[opponent1], opponent1);
		auto analysis2 = analyzeGame(game, die, scores[opponent2], opponent2);
		if (analysis1.bestMoves != analysis2.bestMoves)
		{
			writeln([opponent1, opponent2]);
			writeln(game);
			writeln(analysis1);
			writeln(analysis2);
			break;
		}
	}
}
