import std.algorithm.iteration;
import std.array;
import std.conv;
import std.exception;
import std.random;
import std.stdio;

import ae.sys.data;
import ae.sys.datamm;

import ai;
import alg_common;
import graph;

enum opponent = Opponent.ai4;

immutable ScoreTable scoreTable;
shared static this()
{
	__gshared Data scoreData;
	scoreData = mapFile(scoreFileName(opponent), MmMode.read);
	scoreTable = cast(immutable(Score)[])scoreData.contents;
}

// ---------------------------------------------------------------------------------------

// We want to do calculations incrementally,
// but want to avoid compounding floating-point errors
alias IntegerScore = long;

enum integerScore1 = (IntegerScore(1) << 24);  // 1.0 represented as a IntegerScore

IntegerScore toIntegerScore(real r)
{
	return cast(IntegerScore)(integerScore1 * r);
}

// ---------------------------------------------------------------------------------------

enum numAlgorithms = import("num_algorithms.txt").to!int;

immutable Algorithm[] algorithms;
immutable IntegerScore[] algorithmFullScores;

shared static this()
{
	algorithms = fullEvaluationCache.keys.assumeUnique;
	algorithmFullScores = algorithms
		.map!(a => fullEvaluationCache[a])
		.map!toIntegerScore
		.array
		.assumeUnique;

	if (algorithms.length != numAlgorithms)
	{
		File("num_algorithms.txt", "wb").write(algorithms.length);
		throw new Exception("Algorithm count changed, please rebuild and rerun");
	}
}

// ---------------------------------------------------------------------------------------

enum corpusSize = 1024;

IntegerScore getCorpusScoreFromScoreSums(ref const IntegerScore[numAlgorithms] scoreSums)
{
	IntegerScore result;
	foreach (algorithmIndex, algorithmFullAverage; algorithmFullScores)
	{
		auto corpusAverage = scoreSums[algorithmIndex] / corpusSize;
		auto delta = algorithmFullAverage - corpusAverage;
		result += delta * delta;
		assert(result >= 0, "Overflow");
	}
	return result;
}

struct Corpus
{
	GameUnpacking[corpusSize] games;

	// scores[algorithmIndex][gameIndex] == evaluateAlgorithmForGame(...).toIntegerScore()
	IntegerScore[corpusSize][numAlgorithms] scores;

	// scoreSums[algorithmIndex] == scores[algorithmIndex][].sum()
	IntegerScore[numAlgorithms] scoreSums;

	void recalculate()
	{
		foreach (gameIndex, ref game; games)
		{
			foreach (algorithmIndex, ref algorithm; algorithms)
				scores[algorithmIndex][gameIndex] = evaluateAlgorithmForGame!opponent(algorithm, game, scoreTable).toIntegerScore();
		}
		foreach (algorithmIndex, ref algorithm; algorithms)
			scoreSums[algorithmIndex] = scores[algorithmIndex][].sum();
	}

	IntegerScore getCorpusScore() const
	{
		return getCorpusScoreFromScoreSums(scoreSums);
	}
}

Corpus randomCorpus()
{
	Corpus result;
	foreach (ref game; result.games)
		game = randomGameUnpacking();
	result.recalculate();
	return result;
}

// ---------------------------------------------------------------------------------------

enum maxEdits = 4;

struct CorpusEdit
{
	struct Edit
	{
		size_t gameIndex;
		GameUnpacking newGame;
	}
	Edit[maxEdits] edits;
	size_t numEdits;
}

CorpusEdit randomEdit()
{
	CorpusEdit e;
	e.numEdits = uniform(1, maxEdits+1);
	foreach (editIndex, ref edit; e.edits[0 .. e.numEdits])
	{
		edit.gameIndex = uniform(0, corpusSize);
		edit.newGame = randomGameUnpacking();
	}
	return e;
}

// ---------------------------------------------------------------------------------------

IntegerScore tryEdit(ref const Corpus corpus, ref const CorpusEdit corpusEdit)
{
	IntegerScore newCorpusScore = 0;

	foreach (algorithmIndex, ref algorithm; algorithms)
	{
		IntegerScore scoreSum = corpus.scoreSums[algorithmIndex];

		// Sparsely populate
		// We do this as a separate step because gameIndex may repeat
		IntegerScore[corpusSize] scores = void;
		foreach (editIndex, ref edit; corpusEdit.edits[0 .. corpusEdit.numEdits])
			scores[edit.gameIndex] = corpus.scores[algorithmIndex][edit.gameIndex];

		foreach (editIndex, ref edit; corpusEdit.edits[0 .. corpusEdit.numEdits])
		{
			auto newGameScore = evaluateAlgorithmForGame!opponent(algorithm, edit.newGame, scoreTable).toIntegerScore();
			// operation.editScores[algorithmIndex][editIndex] = newGameScore;

			scoreSum -= scores[edit.gameIndex];
			scoreSum += newGameScore;
			scores[edit.gameIndex] = newGameScore;
		}

		auto algorithmFullAverage = algorithmFullScores[algorithmIndex];
		auto corpusAverage = scoreSum / corpusSize;
		auto delta = algorithmFullAverage - corpusAverage;
		newCorpusScore += delta * delta;
		assert(newCorpusScore >= 0, "Overflow");
	}

	return newCorpusScore;
}

void applyEdit(ref Corpus corpus, ref const CorpusEdit corpusEdit)
{
	foreach (editIndex, ref edit; corpusEdit.edits[0 .. corpusEdit.numEdits])
	{
		auto oldGame = corpus.games[edit.gameIndex];
		auto newGame = edit.newGame;

		foreach (algorithmIndex, ref algorithm; algorithms)
		{
			auto oldScore = corpus.scores[algorithmIndex][edit.gameIndex];
			auto newScore = evaluateAlgorithmForGame!opponent(algorithm, newGame, scoreTable).toIntegerScore();
			corpus.scoreSums[algorithmIndex] -= oldScore;
			corpus.scoreSums[algorithmIndex] += newScore;
			corpus.scores[algorithmIndex][edit.gameIndex] = newScore;
		}

		corpus.games[edit.gameIndex] = newGame;
	}

	debug
	{
		auto corpus2 = corpus;
		corpus2.recalculate();
		assert(corpus == corpus2);
	}
}

// ---------------------------------------------------------------------------------------

struct CorpusResult
{
	enum StopReason { running, checkpoint, stall }
	StopReason stopReason;
	IntegerScore score;
	size_t numAlgorithms;
	size_t iteration;
	GameUnpacking[corpusSize] games;
}
