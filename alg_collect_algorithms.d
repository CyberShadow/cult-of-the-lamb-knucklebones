import std.algorithm.iteration;
import std.algorithm.sorting;
import std.array;
import std.file;
import std.stdio : stderr;

import ae.utils.array;
import ae.utils.json;

import ai;
import alg_common;

Algorithm[] loadAlgorithms()
{
	@JSONPartial
	struct JsonFile
	{
		struct JsonAlgorithm
		{
			Rule[] rules;
		}
		JsonAlgorithm algorithm;
	}

	return
		dirEntries("", "algorithm-*.json", SpanMode.depth)
		.map!(de => de.readText.jsonParse!JsonFile)
		.map!((f) {
			Algorithm a;
			size_t ruleIndex;
			foreach (rule; f.algorithm.rules)
				if (rule != Algorithm.init.rules[0]/* && rule != Rule.init*/)
				{
					if (ruleIndex >= a.rules.length)
						assert(false, "Algorithm has too many rules");
					a.rules[ruleIndex++] = rule;
				}
			return a.normalize();
		})
		.array
		.sort!bytewiseCompare
		.uniq
		.filter!(algorithm => algorithm !in fullEvaluationCache)
		.array;
}

enum algorithmsFileName = "algorithms.json";

void main()
{
	auto allAlgorithms = loadAlgorithms();

	write(algorithmsFileName, allAlgorithms.toJson);
	stderr.writefln("Collected %d new algorithms to %s.", allAlgorithms.length, algorithmsFileName);
}
