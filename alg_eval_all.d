import std.algorithm.iteration;
import std.algorithm.sorting;
import std.array;
import std.exception;
import std.file;
import std.stdio;

import ae.sys.datamm;
import ae.utils.array;
import ae.utils.json;

import ai;
import alg_common;
import graph;

immutable Algorithm[] allAlgorithms = import("algorithms.json").jsonParse!(Algorithm[]);

enum opponent = Opponent.ai4;

void main()
{
	foreach (algorithm; allAlgorithms)
		enforce(algorithm !in fullEvaluationCache, "An algorithm has already been evaluated");

	auto scoreData = mapFile(scoreFileName(opponent), MmMode.read);
	auto scoreTable = cast(immutable(Score)[])scoreData.contents;

	// writeln(allAlgorithms.length);
	auto results = evaluateAlgorithmsInFull!(opponent, allAlgorithms)(scoreTable);

	foreach (i, algorithm; allAlgorithms)
		fullEvaluationCache[algorithm] = results[i];
}
