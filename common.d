import std.algorithm.iteration;
import std.algorithm.mutation : swap;
import std.algorithm.searching;

import ae.utils.meta;

enum Player { current, other }
enum size_t numPlayers = enumLength!Player;

alias Die = ubyte; // 1 to 6, or 0 for none

enum Die dieNone = 0;
enum Die dieMin = 1;
enum Die dieMax = 6;

enum numColumns = 3;
enum numRows = 3;

struct Game
{
	Die[numRows][numColumns][enumLength!Player] board;
}

alias Move = ubyte; // 0..3 to indicate column
enum Move numMoves = numColumns;

bool canPlace(Game game, Move move) @nogc
{
	return game.board[Player.current][move][$-1] == 0;
}

Game place(Game game, Move move, Die die) @nogc
in(canPlace(game, move))
{
	assert(die != 0, "Next die not selected");

	foreach (row; 0 .. numRows)
		if (game.board[Player.current][move][row] == dieNone)
		{
			game.board[Player.current][move][row] = die;

			foreach (i; 0 .. numColumns)
				while (game.board[Player.other][move][i] == die)
					foreach (j; i .. numColumns)
					{
						game.board[Player.other][move][j] =
							(j + 1 == numColumns)
							? dieNone
							: game.board[Player.other][move][j + 1];
					}

			return game;
		}
	assert(false, "Move is not possible");
}

// Call this right after `place`.
bool isGameOver(Game game) @nogc
{
	foreach (column; game.board[Player.current])
		foreach (die; column)
			if (die == dieNone)
				return false;
	return true;
}

Game flipBoard(Game game) @nogc
{
	swap(game.board[Player.current], game.board[Player.other]);
	return game;
}

size_t getNumDiceInColumn(Die[numRows] dice) @nogc
{
	return dice[].countUntil(dieNone);
}

size_t getNumDiceOnPlayerBoard(Die[numRows][numColumns] board) @nogc
{
	return board[].map!getNumDiceInColumn.sum;
}

int getColumnScore(Die[numRows] dice) @nogc
{
	int score;
	ubyte[6+1] sawDie;
	foreach (row; 0 .. numRows)
	{
		auto die = dice[row];
		if (die == dieNone)
		{
			foreach (j; row + 1 .. numRows)
				assert(dice[j] == dieNone, "Bad column");
			break;
		}
		score += die;
		if (sawDie[die] == 1)
			score += die * 2; // Total is x 2 (dice) x 2 (bonus)
		else
		if (sawDie[die] == 2)
			score += die * 4; // Total is x 3 (dice) x 3 (bonus)
		sawDie[die]++;
	}

	return score;
}

int getPlayerScore(Die[numRows][numColumns] board) @nogc
{
	return board[].map!getColumnScore.sum;
}

int[enumLength!Player] getPlayerScores(Game game) @nogc
{
	int[enumLength!Player] playerScores;
	foreach (player; Player.init .. enumLength!Player)
		playerScores[player] = game.board[player].getPlayerScore();
	return playerScores;
}

size_t getNumMatchesInColumn(Die[numRows] dice, Die rolledDie) @nogc
{
	size_t result = 0;
	foreach (die; dice)
		if (die == rolledDie)
			result++;
	return result;
}
