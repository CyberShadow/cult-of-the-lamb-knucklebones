import std.parallelism;
import std.range;
import std.stdio;

import ae.sys.data;
import ae.sys.datamm;

import packing;
import common;
import graph;

void main()
{
	auto states = new ubyte[packedGameMax];
	states[0] = 1;
	ulong numStates;
	bool foundStates;
	do
	{
		foundStates = false;

		foreach (packedGame; packedGameMax.iota/*.parallel*/)
		{
			if (states[packedGame] != 1)
				continue;

			states[packedGame] = 2;
			numStates++;
			foundStates = true;

			auto game = packedGame.unpackGame();
			if (game.isGameOver || game.flipBoard.isGameOver)
				continue;

			foreach (Die die; dieMin .. dieMax + 1)
				foreach (Move move; 0 .. numMoves)
					if (game.canPlace(move))
					{
						auto game2 = game.place(move, die);
						auto nextGame = game2.flipBoard.packGame;
						if (states[nextGame] == 0)
							states[nextGame] = 1;
					}
		}
		writeln(numStates);
	}
	while (foundStates);
}
