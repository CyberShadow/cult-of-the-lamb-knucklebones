import std.algorithm.searching;
import std.algorithm.sorting;
import std.file;
import std.parallelism;
import std.range;

import ae.sys.data;
import ae.sys.datamm;
import ae.sys.file;
import ae.utils.math.combinatorics;

import common;

// ------------------------- Columns

alias PackedColumn = ushort;

immutable Die[6][] packedColumnOrder;
immutable PackedColumn[7^^6] packedColumnLookup;
enum packedColumnMax = 3067;

shared static this()
{
	foreach (ubyte a0; 0 .. 6+1)
	foreach (ubyte a1; a0 .. 6+1)
	foreach (ubyte a2; a1 .. 6+1)
	foreach (ubyte b0; 0 .. 6+1) if (b0 == 0 || (b0 != a0 && b0 != a1 && b0 != a2))
	foreach (ubyte b1; b0 .. 6+1) if (b1 == 0 || (b1 != a0 && b1 != a1 && b1 != a2))
	foreach (ubyte b2; b1 .. 6+1) if (b2 == 0 || (b2 != a0 && b2 != a1 && b2 != a2))
		packedColumnOrder ~= [a0, a1, a2, b0, b1, b2];
	assert(packedColumnOrder.length == packedColumnMax);

	enum packedColumnLookupFileName = "data/packedColumnLookup.u16";
	if (!packedColumnLookupFileName.exists)
	{
		PackedColumn[7^^6] packedColumnLookup = PackedColumn.max;
		foreach (numericDice; packedColumnLookup.length.iota.parallel)
		{
			Die[6] dice;
			auto rem = numericDice;
			foreach_reverse (ref die; dice)
			{
				die = rem % 7;
				rem /= 7;
			}
			dice[0..3].sort();
			dice[3..6].sort();
			packedColumnLookup[numericDice] = cast(PackedColumn)packedColumnOrder.countUntil(dice);
		}
		atomicWrite(packedColumnLookupFileName, packedColumnLookup[]);
	}

	static Data packedColumnData;
	packedColumnData = mapFile(packedColumnLookupFileName, MmMode.read);
	.packedColumnLookup = (cast(PackedColumn[])packedColumnData.contents)[0 .. .packedColumnLookup.length];
}

Die[6] unpackColumn(PackedColumn c) @nogc { return packedColumnOrder[c]; }

PackedColumn packColumn(Die[6] dice) @nogc
{
	uint numericDice = 0;
	foreach (die; dice)
		numericDice = numericDice * 7 + die;
	auto packed = packedColumnLookup[numericDice];
	assert(packed != PackedColumn.max, "Unrepresentable column");
	return packed;
}

version (none)
void main()
{
	import std.stdio : writeln;
	writeln(packedColumnOrder.length);
	writeln(packColumn([1, 0, 0, 0, 0, 0]));
	writeln(unpackColumn(84));
}

// ------------------------- Game

alias PackedGame = ulong;

static immutable gameBinomials = BinomialCoefficientTable!(PackedGame, packedColumnMax + 1, numColumns).generate();
alias GameCNS = CNS!(PackedGame, PackedColumn, numColumns, packedColumnMax, /*lexicographic:*/false, /*multiset:*/true, gameBinomials);
enum packedGameMax = GameCNS.packedCard;
static assert (packedGameMax == 4812987894); // 3067 multichoose 3

PackedGame packGame(Game game) @nogc
{
	PackedColumn[3] columns;
	foreach (columnIndex, ref column; columns)
	{
		column = packColumn([
			game.board[Player.current][columnIndex][2],
			game.board[Player.current][columnIndex][1],
			game.board[Player.current][columnIndex][0],
			game.board[Player.other  ][columnIndex][2],
			game.board[Player.other  ][columnIndex][1],
			game.board[Player.other  ][columnIndex][0],
		]);
	}
	columns[].sort();
	return GameCNS.pack(columns);
}

Game unpackGame(PackedGame packed) @nogc
{
	auto columns = GameCNS.unpack(packed);

	Game game;
	foreach (columnIndex, packedColumn; columns)
	{
		auto column = unpackColumn(packedColumn);
		game.board[Player.current][columnIndex][2] = column[0];
		game.board[Player.current][columnIndex][1] = column[1];
		game.board[Player.current][columnIndex][0] = column[2];
		game.board[Player.other  ][columnIndex][2] = column[3];
		game.board[Player.other  ][columnIndex][1] = column[4];
		game.board[Player.other  ][columnIndex][0] = column[5];
	}
	return game;
}
