import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.mutation;
import std.algorithm.searching;
import std.algorithm.sorting;
import std.parallelism;
import std.random;
import std.range;

import ae.sys.data;
import ae.sys.datamm;
import ae.sys.persistence.keyvalue;
import ae.utils.array;
import ae.utils.meta;

import common;
import graph;
import packing;
import ai;

alias RNG = Xorshift;

enum numRules = 8;
enum numConditions = 2;

alias bytewiseCompare = (ref a, ref b) => a.bytes < b.bytes;

struct Condition
{
	enum Type : ubyte
	{
		always, // no-op

		thisColumnIsBestGreedyColumn,

		numDiceInMyColumnEquals,
		numDiceInMyColumnIsMoreThan,
		numDiceInTheirColumnEquals,
		numDiceInTheirColumnIsMoreThan,

		scoreInMyColumnIsMoreThan,
		scoreInTheirColumnIsMoreThan,
		scoreInColumnDifferenceIsMoreThan,

		scoreInMyColumnAfterPlacingIsMoreThan,
		scoreInTheirColumnAfterPlacingIsMoreThan,
		scoreInColumnAfterPlacingDifferenceIsMoreThan,

		numDiceOnMyBoardIsMoreThan,
		numDiceOnTheirBoardIsMoreThan,
		numDiceDifferenceIsMoreThan,
		numDiceOnBoardIsMoreThan,

		scoreOnMyBoardIsMoreThan,
		scoreOnTheirBoardIsMoreThan,
		scoreOnBoardDifferenceIsMoreThan,

		scoreOnMyBoardAfterPlacingIsMoreThan,
		scoreOnTheirBoardAfterPlacingIsMoreThan,
		scoreOnBoardAfterPlacingDifferenceIsMoreThan,

		scoreOnBoardDifferenceChangeByPlacingIsMoreThan,

		theRolledDieEquals,
		theRolledDieIsMoreThan,

		matchesWithTheRolledDieInMyColumnEquals,
		matchesWithTheRolledDieInMyColumnIsMoreThan,
		matchesWithTheRolledDieInTheirColumnEquals,
		matchesWithTheRolledDieInTheirColumnIsMoreThan,
	}
	Type type;
	byte argument;
	bool not;

	enum Condition always = Condition(Type.always, 0, false);
	enum Condition never = Condition(Type.always, 0, true);

	Condition normalize() const @nogc
	{
		Condition c = this;
		switch (type)
		{
			case Condition.Type.always:
			case Condition.Type.thisColumnIsBestGreedyColumn:
				c.argument = 0;
				break;
			default:
		}
		return c;
	}
}

Condition genCondition(ref RNG rng)
{
	Condition c;
	c.type = uniform(Condition.Type.init, enumLength!(Condition.Type), rng);
	c.argument = cast(byte) {
		final switch (c.type)
		{
			case Condition.Type.always:
			case Condition.Type.thisColumnIsBestGreedyColumn:
				return 0;

			case Condition.Type.numDiceInMyColumnEquals:
			case Condition.Type.numDiceInTheirColumnEquals:
				return uniform(0, 4, rng);
			case Condition.Type.numDiceInMyColumnIsMoreThan:
			case Condition.Type.numDiceInTheirColumnIsMoreThan:
				return uniform(0, 3, rng);

			case Condition.Type.scoreInMyColumnIsMoreThan:
			case Condition.Type.scoreInTheirColumnIsMoreThan:
			case Condition.Type.scoreInMyColumnAfterPlacingIsMoreThan:
			case Condition.Type.scoreInTheirColumnAfterPlacingIsMoreThan:
				return uniform(0, 9*6, rng);
			case Condition.Type.scoreInColumnDifferenceIsMoreThan:
			case Condition.Type.scoreInColumnAfterPlacingDifferenceIsMoreThan:
				return uniform(-9*6, 9*6, rng);

			case Condition.Type.numDiceOnMyBoardIsMoreThan:
			case Condition.Type.numDiceOnTheirBoardIsMoreThan:
				return uniform( 0, 9, rng);
			case Condition.Type.numDiceDifferenceIsMoreThan:
				return uniform(-9, 9, rng);
			case Condition.Type.numDiceOnBoardIsMoreThan:
				return uniform( 0, 9*2, rng);

			case Condition.Type.scoreOnMyBoardIsMoreThan:
			case Condition.Type.scoreOnTheirBoardIsMoreThan:
			case Condition.Type.scoreOnMyBoardAfterPlacingIsMoreThan:
			case Condition.Type.scoreOnTheirBoardAfterPlacingIsMoreThan:
				return uniform(0, 127, rng); // Highest possible score is actually 3*9*6 == 162
			case Condition.Type.scoreOnBoardDifferenceIsMoreThan:
			case Condition.Type.scoreOnBoardAfterPlacingDifferenceIsMoreThan:
			case Condition.Type.scoreOnBoardDifferenceChangeByPlacingIsMoreThan:
				return uniform(-127, 127, rng);

			case Condition.Type.theRolledDieEquals:
				return uniform(1, 6+1, rng);
			case Condition.Type.theRolledDieIsMoreThan:
				return uniform(0, 6, rng);

			case Condition.Type.matchesWithTheRolledDieInMyColumnEquals:
			case Condition.Type.matchesWithTheRolledDieInTheirColumnEquals:
				return uniform(0, 4, rng);
			case Condition.Type.matchesWithTheRolledDieInMyColumnIsMoreThan:
			case Condition.Type.matchesWithTheRolledDieInTheirColumnIsMoreThan:
				return uniform(0, 3, rng);
		}
	}();
	c.not = uniform(0, 2, rng) == 1;
	return c;
}

struct Rule
{
	// These will be combined with "and".
	// If all are true, place in this column.
	Condition[numConditions] conditions = Condition.always;

	// No-op
	enum Rule never = {
		Rule r;
		r.conditions[] = Condition.never;
		return r;
	}();

	// Terminal
	enum Rule always = {
		Rule r;
		r.conditions[] = Condition.always;
		return r;
	}();

	Rule normalize() const @nogc
	{
		Rule r = this;
		foreach (ref c; r.conditions)
			c = c.normalize();
		r.conditions[].sort!bytewiseCompare();
		return r;
	}
}

Rule genRule(ref RNG rng)
{
	Rule r;
	foreach (ref condition; r.conditions[0 .. uniform(1, numConditions+1, rng)])
		condition = genCondition(rng);
	return r;
}

struct Algorithm
{
	Rule[numRules] rules = Rule.never;

	Algorithm normalize() const @nogc
	{
		Algorithm a;
		size_t numRules;
		foreach (Rule r; this.rules)
		{
			r = r.normalize();
			if (r != Rule.never)
				a.rules[numRules++] = r;
			if (r == Rule.always)
				break;
		}
		return a;
	}
}

Algorithm genAlgorithm(ref RNG rng)
{
	Algorithm algorithm;
	foreach (ref rule; algorithm.rules)
		rule = genRule(rng);
	return algorithm;
}

void evaluate(alias algorithms)(const Game game, Die rolledDie, /*out*/ Move[] moves) @nogc
{
	assert(moves.length == algorithms.length);

	bool[numMoves] moveValid;
	foreach (Move move; 0 .. numMoves)
		moveValid[move] = game.canPlace(move);

	Game[numMoves] gamesAfterMove;
	foreach (Move move; 0 .. numMoves)
		if (moveValid[move])
			gamesAfterMove[move] = game.place(move, rolledDie);

	int[enumLength!Player] playerScores = game.getPlayerScores();

	int[enumLength!Player][numMoves] playerScoresAfterMove;
	foreach (Move move; 0 .. numMoves)
		if (moveValid[move])
			playerScoresAfterMove[move] = gamesAfterMove[move].getPlayerScores();

	int[numMoves] playerScoreDifferencesAfterMove;
	foreach (Move move; 0 .. numMoves)
		if (moveValid[move])
			playerScoreDifferencesAfterMove[move] = playerScoresAfterMove[move][Player.current] - playerScoresAfterMove[move][Player.other];

	int bestPlayerScoreDifferenceAfterMove = int.min;
	foreach (Move move; 0 .. numMoves)
		if (moveValid[move])
			bestPlayerScoreDifferenceAfterMove = max(bestPlayerScoreDifferenceAfterMove, playerScoreDifferencesAfterMove[move]);

	uint[numColumns][numPlayers] numDiceInColumn;
	foreach (player; Player.init .. enumLength!Player)
		foreach (column; 0 .. numColumns)
			numDiceInColumn[player][column] = cast(uint)getNumDiceInColumn(game.board[player][column]);

	int[numColumns][numPlayers] scoreInColumn;
	foreach (player; Player.init .. enumLength!Player)
		foreach (column; 0 .. numColumns)
			scoreInColumn[player][column] = game.board[player][column].getColumnScore();

	// int[numColumns][numPlayers][numMoves] scoreInColumnAfterMove;
	// foreach (Move move; 0 .. numMoves)
	// 	foreach (player; Player.init .. enumLength!Player)
	// 		foreach (column; 0 .. numColumns)
	// 			scoreInColumnAfterMove[move][player][column] = gamesAfterMove[move].board[player][column].getColumnScore();

	int[numColumns][numPlayers] scoreInColumnAfterMove;
	foreach (player; Player.init .. enumLength!Player)
		foreach (column; 0 .. numColumns)
			scoreInColumnAfterMove[player][column] = gamesAfterMove[column].board[player][column].getColumnScore();

	// uint[numPlayers] minDiceInColumns;
	// foreach (player; Player.init .. enumLength!Player)
	// 	minDiceInColumns[player] = min(
	// 		diceInColumn[player][0],
	// 		diceInColumn[player][1],
	// 		diceInColumn[player][2],
	// 	);

	size_t[numPlayers] numDiceOnPlayerBoard;
	foreach (player; Player.init .. enumLength!Player)
		numDiceOnPlayerBoard[player] = getNumDiceOnPlayerBoard(game.board[player]);

	size_t[numColumns][numPlayers] numMatchesInColumn;
	foreach (player; Player.init .. enumLength!Player)
		foreach (column; 0 .. numColumns)
			numMatchesInColumn[player][column] = game.board[player][column].getNumMatchesInColumn(rolledDie);

	bool evaluateCondition(alias /*Condition*/ c)(Move move)
	{
		assert(moveValid[move]);
		auto result = {
			final switch (c.type)
			{
				case Condition.Type.always:
					return true;

				case Condition.Type.thisColumnIsBestGreedyColumn:
					return playerScoreDifferencesAfterMove[move] == bestPlayerScoreDifferenceAfterMove;

				case Condition.Type.numDiceInMyColumnEquals:
					return numDiceInColumn[Player.current][move] == c.argument;
				case Condition.Type.numDiceInMyColumnIsMoreThan:
					return numDiceInColumn[Player.current][move] > c.argument;
				case Condition.Type.numDiceInTheirColumnEquals:
					return numDiceInColumn[Player.other][move] == c.argument;
				case Condition.Type.numDiceInTheirColumnIsMoreThan:
					return numDiceInColumn[Player.other][move] > c.argument;

				case Condition.Type.scoreInMyColumnIsMoreThan:
					return scoreInColumn[Player.current][move] > c.argument;
				case Condition.Type.scoreInTheirColumnIsMoreThan:
					return scoreInColumn[Player.other][move] > c.argument;
				case Condition.Type.scoreInColumnDifferenceIsMoreThan:
					return scoreInColumn[Player.current][move] - scoreInColumn[Player.other][move] > c.argument;

				case Condition.Type.scoreInMyColumnAfterPlacingIsMoreThan:
					return scoreInColumnAfterMove[Player.current][move] > c.argument;
				case Condition.Type.scoreInTheirColumnAfterPlacingIsMoreThan:
					return scoreInColumnAfterMove[Player.other][move] > c.argument;
				case Condition.Type.scoreInColumnAfterPlacingDifferenceIsMoreThan:
					return scoreInColumnAfterMove[Player.current][move] - scoreInColumnAfterMove[Player.other][move] > c.argument;

				case Condition.Type.numDiceOnMyBoardIsMoreThan:
					return numDiceOnPlayerBoard[Player.current] > c.argument;
				case Condition.Type.numDiceOnTheirBoardIsMoreThan:
					return numDiceOnPlayerBoard[Player.other] > c.argument;
				case Condition.Type.numDiceDifferenceIsMoreThan:
					return numDiceOnPlayerBoard[Player.current] - numDiceOnPlayerBoard[Player.other] > c.argument;
				case Condition.Type.numDiceOnBoardIsMoreThan:
					return numDiceOnPlayerBoard[Player.current] + numDiceOnPlayerBoard[Player.other] > c.argument;

				case Condition.Type.scoreOnMyBoardIsMoreThan:
					return playerScores[Player.current] > c.argument;
				case Condition.Type.scoreOnTheirBoardIsMoreThan:
					return playerScores[Player.other] > c.argument;
				case Condition.Type.scoreOnBoardDifferenceIsMoreThan:
					return playerScores[Player.current] - playerScores[Player.other] > c.argument;

				case Condition.Type.scoreOnMyBoardAfterPlacingIsMoreThan:
					return playerScoresAfterMove[move][Player.current] > c.argument;
				case Condition.Type.scoreOnTheirBoardAfterPlacingIsMoreThan:
					return playerScoresAfterMove[move][Player.other] > c.argument;
				case Condition.Type.scoreOnBoardAfterPlacingDifferenceIsMoreThan:
					return playerScoresAfterMove[move][Player.current] - playerScoresAfterMove[move][Player.other] > c.argument;

				case Condition.Type.scoreOnBoardDifferenceChangeByPlacingIsMoreThan:
					return
						(playerScoresAfterMove[move][Player.current] - playerScoresAfterMove[move][Player.other])
						-
						(playerScores[Player.current] - playerScores[Player.other])
					 	> c.argument;

				case Condition.Type.theRolledDieEquals:
					return rolledDie == c.argument;
				case Condition.Type.theRolledDieIsMoreThan:
					return rolledDie > c.argument;

				case Condition.Type.matchesWithTheRolledDieInMyColumnEquals:
					return numMatchesInColumn[Player.current][move] == c.argument;
				case Condition.Type.matchesWithTheRolledDieInMyColumnIsMoreThan:
					return numMatchesInColumn[Player.current][move] > c.argument;
				case Condition.Type.matchesWithTheRolledDieInTheirColumnEquals:
					return numMatchesInColumn[Player.other][move] == c.argument;
				case Condition.Type.matchesWithTheRolledDieInTheirColumnIsMoreThan:
					return numMatchesInColumn[Player.other][move] > c.argument;
			}
		}();
		if (c.not)
			result = !result;
		return result;
	}

	static if (is(typeof({ enum x = algorithms.length; enum y = algorithms[0]; })))
	{
		pragma(msg, "Using compile-time algorithm code generation");

		static foreach (i, algorithm; algorithms)
			moves[i] = {
				static foreach (rule; algorithm.rules)
					foreach (Move move; 0 .. numMoves)
						if (moveValid[move])
						{
							bool ruleMatched = {
								static foreach (condition; rule.conditions)
									if (!evaluateCondition!condition(move))
										return false;
								return true; // All conditions are true
							}();
							if (ruleMatched)
								return move;
						}

				// No rules matched, move in first available column

				foreach_reverse (Move move; 0 .. numMoves)
					if (moveValid[move])
						return move;

				assert(false, "No moves");
			}();
	}
	else
	{
		pragma(msg, "Evaluating algorithm at runtime");

		foreach (i, ref algorithm; algorithms)
			moves[i] = {
				foreach (ref rule; algorithm.rules)
					foreach (Move move; 0 .. numMoves)
						if (moveValid[move])
						{
							bool ruleMatched = {
								foreach (condition; rule.conditions)
								{
									static Condition condition_;
									condition_ = condition;
									if (!evaluateCondition!condition_(move))
										return false;
								}
								return true; // All conditions are true
							}();
							if (ruleMatched)
								return move;
						}

				// No rules matched, move in first available column

				foreach_reverse (Move move; 0 .. numMoves)
					if (moveValid[move])
						return move;

				assert(false, "No moves");
			}();
	}
}

Move evaluate(const Game game, Die rolledDie, ref const Algorithm algorithm) @nogc
{
	Move[1] move;
	auto algorithms = (&algorithm)[0..1];
	evaluate!algorithms(game, rolledDie, move[]);
	return move[0];
}

immutable ubyte[3][6] columnOrders = [
	[0, 1, 2],
	[0, 2, 1],
	[1, 0, 2],
	[1, 2, 0],
	[2, 0, 1],
	[2, 1, 0],
];

/// A packed game and additional instructions for unpacking which would affect algorithms.
struct GameUnpacking
{
	PackedGame packedGame;
	ubyte columnOrder;
	Die rolledDie;

	Game unpack() const @nogc
	{
		auto game = this.packedGame.unpackGame();

		Game game2;
		foreach (player; Player.init .. numPlayers)
			foreach (column; 0 .. numColumns)
				game2.board[player][column] = game.board[player][columnOrders[this.columnOrder][column]];

		return game2;
	}
}

GameUnpacking randomGameUnpacking()
{
	GameUnpacking result;
	result.packedGame = uniform(0, packedGameMax);
	result.columnOrder = uniform(ubyte(0), ubyte(columnOrders.length));
	result.rolledDie = uniform(dieMin, ubyte(dieMax + 1));
	return result;
}

/// Returns error (deviation from optimal move's score).
float evaluateAlgorithmForGame(Opponent opponent)(ref const Algorithm algorithm, GameUnpacking gameUnpacking, const ScoreTable scoreTable) @nogc
{
	auto game = gameUnpacking.unpack();
	auto die = gameUnpacking.rolledDie;

	if (game.isGameOver || game.flipBoard.isGameOver)
		return 0;
	else
	{
		Score[numMoves] moveScores;
		Score bestScore = minScore;

		foreach (Move move; 0 .. numMoves)
			if (game.canPlace(move))
			{
				auto nextGame = game.place(move, die).flipBoard;
				Score moveScore = -getOpponentScore!opponent(nextGame, scoreTable);
				moveScores[move] = moveScore;
				if (moveScore > bestScore)
					bestScore = moveScore;
			}
		if (bestScore == minScore)
			assert(false, "No moves available?");

		auto algMove = evaluate(game, die, algorithm);
		auto delta = bestScore - moveScores[algMove];
		assert(delta >= 0);
		return delta;
	}
}

real[] evaluateAlgorithmsInFull(Opponent opponent, alias algorithms)(const ScoreTable scoreTable) // @nogc
{
	real[] sums = new real[algorithms.length];
	sums[] = 0;

	PackedGame count;

	foreach (chunk; packedGameMax.iota.chunks(16384).parallel(1))
	{
		real[] chunkSums = new real[algorithms.length];
		chunkSums[] = 0;

		foreach (PackedGame packedGame; chunk)
		{
			if (false)
			{
				// Equivalent to the below, but below version is much faster
				foreach (ubyte columnOrder; 0 .. columnOrders.length)
					foreach (Die rolledDie; dieMin .. dieMax + 1)
						foreach (i, ref algorithm; algorithms)
							chunkSums[i] += evaluateAlgorithmForGame!opponent(algorithm, GameUnpacking(packedGame, columnOrder, rolledDie), scoreTable);
			}
			else
			{
				auto game = packedGame.unpackGame();
				if (game.isGameOver || game.flipBoard.isGameOver)
					continue;
				else
				{
					foreach (Die die; dieMin .. dieMax + 1)
					{
						Score[numMoves] moveScores;
						Score bestScore = minScore;

						foreach (Move move; 0 .. numMoves)
							if (game.canPlace(move))
							{
								auto nextGame = game.place(move, die).flipBoard;
								Score moveScore = -getOpponentScore!opponent(nextGame, scoreTable);
								moveScores[move] = moveScore;
								if (moveScore > bestScore)
									bestScore = moveScore;
							}
						if (bestScore == minScore)
							assert(false, "No moves available?");

						foreach (columnOrder; columnOrders)
						{
							Game game2;
							foreach (player; Player.init .. numPlayers)
								foreach (column; 0 .. numColumns)
									game2.board[player][column] = game.board[player][columnOrder[column]];

							static Move[] algMoves;
							if (algMoves.length != algorithms.length)
								algMoves.length = algorithms.length;

							evaluate!algorithms(game2, die, algMoves);
							foreach (i, algMove; algMoves)
							{
								algMove = columnOrder[algMove]; // unshuffle

								auto delta = bestScore - moveScores[algMove];
								assert(delta >= 0);
								chunkSums[i] += delta;
							}
						}
					}
				}
			}
		}

		synchronized
		{
			sums[] += chunkSums[];

			count += chunk.length;
			import std.stdio : stderr;
			stderr.writefln("%d / %d (%d%%)", count, packedGameMax, 100 * count / packedGameMax);
		}
	}
	sums[] /= (packedGameMax * columnOrders.length * 6);
	return sums;
}

real evaluateAlgorithmInFull(Opponent opponent)(ref const Algorithm algorithm, const ScoreTable scoreTable)
{
	auto algorithms = (&algorithm)[0..1];
	return evaluateAlgorithmsInFull!(opponent, algorithms)(scoreTable)[0];
}
	
unittest
{
	Algorithm a;
	if (false)
		cast(void) evaluateAlgorithmInFull!(Opponent.init)(a, null);
}

KeyValueStore!(Algorithm, real) fullEvaluationCache;
shared static this()
{
	fullEvaluationCache = typeof(fullEvaluationCache)("algorithm-full-evaluation.s3db");
}

// EvaluationResult evaluateAlgorithm(ref const Algorithm algorithm, size_t iteration)
// {
// 	float analyzeGames(R)(R packedGames)
// 	{
// 		return packedGames.map!analyzeGame
// 			.I!(r => r.sum / r.length);
// 	}

// 	enum step = 120000;
// 	auto offset = iteration % step;

// 	// return iota(0, packedGameMax, step)
// 	// 	.I!(r => r.chunks(r.length / totalCPUs + 1))
// 	// 	.I!(r => taskPool.amap!analyzeGames(r))
// 	// 	.I!(r => r.sum / r.length);

// 	float score = 0;

// 	auto avgError = iota(offset, packedGameMax, step).I!analyzeGames;
// 	score += avgError;

// 	enum float complexityMalus = 1f/128;
// 	foreach (rule; algorithm.rules)
// 		if (rule != Rule.never)
// 			foreach (condition; rule.conditions)
// 				if (condition != Condition.always)
// 					score += (complexityMalus / numRules / numConditions);

// 	return EvaluationResult(
// 		score,
// 		avgError,
// 	);
// }

void mutateAlgorithm(ref Algorithm algorithm, ref RNG rng)
{
	enum maxMutations = numRules / 2;
	foreach (m; 0 .. uniform(1, maxMutations, rng))
	{
		[
			// Add / overwrite rule
			{
				algorithm.rules[uniform(0, $, rng)] = genRule(rng);
			},
			// Clear rule
			{
				algorithm.rules[uniform(0, $, rng)] = Rule.never;
			},
			// Swap rules
			{
				swap(algorithm.rules[uniform(0, $, rng)], algorithm.rules[uniform(0, $, rng)]);
			},
			// Add / overwrite condition
			{
				algorithm.rules[uniform(0, $, rng)].conditions[uniform(0, $, rng)] = genCondition(rng);
			},
			// Clear condition
			{
				algorithm.rules[uniform(0, $, rng)].conditions[uniform(0, $, rng)] = Condition.always;
			},
			// Nudge condition argument
			{
				algorithm.rules[uniform(0, $, rng)].conditions[uniform(0, $, rng)].argument += uniform(0, 2, rng) * 2 - 1;
			},
		][uniform(0, $, rng)]();
	}
}
