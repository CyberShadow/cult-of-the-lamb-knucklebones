import std.algorithm.sorting;
import std.conv;
import std.exception;
import std.file;
import std.path;

import ae.net.asockets;
import ae.net.http.client;
import ae.utils.aa;
import ae.utils.array;

import ai : Opponent;
import graph : Score, scoreFileName;
import packing : PackedGame;

final class RemoteScores
{
private:
	Score[PackedGame] resolved;
	HashSet!PackedGame todo;
	string url;

public:
	this(Opponent opponent)
	{
		url = "~/.config/knucklebones-data".expandTilde().readText()
			~ scoreFileName(opponent).baseName;
	}

	Score opIndex(PackedGame game)
	{
		if (auto p_score = game in resolved)
			return *p_score;
		todo.add(game);
		return Score.nan; // placeholder value
	}

	// Returns true if resolution succeeded entirely;
	// false, if a rerun is needed.
	bool resolve()
	{
		if (todo.empty)
			return true;

		auto games = todo.keys.sort.release;
		todo = null;

		auto client = new HttpsClient;
		client.keepAlive = true;
		client.pipelining = true;
		client.agent = null;
		client.handleResponse = (HttpResponse response, string disconnectReason) {
			enforce(response, disconnectReason);
			enforce(response.data.bytes.length == Score.sizeof, "Bad payload size");
			Score score;
			response.data.copyTo(score.toArray);
			resolved.addNew(games.shift(), score).enforce();
			if (!games.length)
				client.disconnect();
		};
		foreach (game; games)
		{
			auto request = new HttpRequest(url);
			request.headers["Range"] = "bytes=" ~ text(game * Score.sizeof) ~ "-" ~ text(game * Score.sizeof + Score.sizeof - 1);
			client.request(request);
		}

		socketManager.loop();

		assert(!games.length);

		return false;
	}
}

// Debug helper (assert/invariant guard)
bool scoresAreFinal(Scores)(Scores scores)
{
	static if (is(Scores == RemoteScores))
		return scores.todo.byKey.empty;
	else
		return true;
}
