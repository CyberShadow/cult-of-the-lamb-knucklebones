import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.mutation;
import std.conv;
import std.digest.crc;
import std.file;
import std.format;
import std.math.algebraic;
import std.math.operations;
import std.parallelism;
import std.range;
import std.traits;

import ae.sys.data;
import ae.sys.dataio;
import ae.sys.file;
import ae.utils.meta;
import ae.utils.text;

import ai;
import common;
import packing;
import remotegraph : scoresAreFinal;

alias Score = float;
enum Score scoreWin     =  1f;
enum Score scoreLose    = -1f;
enum Score scoreDraw    =  0f;
enum Score scoreInitial =  0f;
enum Score minScore = -Score.max;

alias ScoreTable = Score[/*packedGameMax*/];

Score getGameOverScore(Game game) @nogc
{
	assert(game.isGameOver || game.flipBoard.isGameOver);
	auto scores = game.getPlayerScores();
	if (scores[Player.current] > scores[Player.other])
		return scoreWin;
	else
	if (scores[Player.current] < scores[Player.other])
		return scoreLose;
	else
		return scoreDraw;
}

void initializeScores(ScoreTable target)
{
	foreach (packedGame; packedGameMax.iota.parallel(packedGameMax / totalCPUs / 128))
	{
		auto game = packedGame.unpackGame();
		Score score;
		if (game.isGameOver || game.flipBoard.isGameOver)
			score = game.getGameOverScore();
		else
			score = scoreInitial;
		target[packedGame] = score;
	}
}

Score getAIScore(Scores)(Game game, Scores scoreTable, float difficulty) @nogc
{
	// For the non-AI opponents, this is handled by looking up in the table.
	// Because we're skipping two moves ahead (table lookup wise),
	// we have to check this here too.
	if (game.isGameOver || game.flipBoard.isGameOver)
		return game.getGameOverScore();

	enum otherOpponent = Opponent.perfect;

	Score scoreSum = 0;
	foreach (Die die; dieMin .. dieMax + 1)
	{
		auto moveProbabilities = getAIMoveProbabilities(game, die, difficulty);
		foreach (Move move; 0 .. numMoves)
			if (moveProbabilities[move])
			{
				assert(game.canPlace(move));
				auto nextGame = game.place(move, die).flipBoard;
				auto moveScore = -getOpponentScore!otherOpponent(nextGame, scoreTable);
				scoreSum += moveScore * moveProbabilities[move];
			}
	}

	auto avgScore = scoreSum / 6;
	if (scoreTable.scoresAreFinal)
		assert(avgScore >= scoreLose && avgScore <= scoreWin);
	return avgScore;
}

Score getOpponentScore(Opponent opponent, Scores)(Game game, Scores scoreTable) @nogc
{
	final switch (opponent)
	{
		case Opponent.perfect:
			return scoreTable[game.packGame];
		case Opponent.ai1:
			return getAIScore(game, scoreTable, 1);
		case Opponent.ai2:
			return getAIScore(game, scoreTable, 3);
		case Opponent.ai3:
			return getAIScore(game, scoreTable, 5);
		case Opponent.ai4:
			return getAIScore(game, scoreTable, 8);
	}
}

// dynamic -> static dispatcher
Score getOpponentScore(Scores)(Opponent opponent, Game game, Scores scoreTable)
{
	final switch (opponent)
	{
		static foreach (staticOpponent; EnumMembers!Opponent)
		{
			case staticOpponent:
				return getOpponentScore!staticOpponent(game, scoreTable);
		}
	}
}

void propagateScores(Opponent opponent)(const ScoreTable source, ScoreTable target)
{
	foreach (packedGame; packedGameMax.iota.parallel(packedGameMax / totalCPUs / 128))
	{
		auto game = packedGame.unpackGame();
		Score score;
		if (game.isGameOver || game.flipBoard.isGameOver)
			score = source[packedGame];
		else
		{
			Score scoreSum = 0;
			foreach (Die die; dieMin .. dieMax + 1)
			{
				Score bestScore = minScore;
				foreach (Move move; 0 .. numMoves)
					if (game.canPlace(move))
					{
						auto nextGame = game.place(move, die).flipBoard;
						Score moveScore = -getOpponentScore!opponent(nextGame, source);
						if (moveScore > bestScore)
							bestScore = moveScore;
					}
				if (bestScore == minScore)
					assert(false, "No moves available?");
				scoreSum += bestScore;
			}
			score = scoreSum / 6;
		}
		target[packedGame] = score;
	}
}

struct Analysis { Score currentScore; Move[] bestMoves; Score[numMoves] scores; }
Analysis analyzeGame(Scores)(Game game, Die rolledDie, Scores scores, Opponent opponent)
{
	Analysis result;
	result.currentScore = scores[game.packGame];
	if (rolledDie)
	{
		Score bestScore = minScore;
		foreach (Move move; 0 .. numMoves)
			if (game.canPlace(move))
			{
				auto nextGame = game.place(move, rolledDie).flipBoard;
				Score moveScore = -getOpponentScore(opponent, nextGame, scores);
				result.scores[move] = moveScore;
				if (moveScore.isClose(bestScore))
					result.bestMoves ~= move;
				else
				if (moveScore > bestScore)
				{
					bestScore = moveScore;
					result.bestMoves = [move];
				}
			}
		if (scores.scoresAreFinal && bestScore == minScore)
			assert(false, "No moves available?");
	}

	return result;
}

unittest
{
	if (false)
	{
		const ScoreTable scores;
		cast(void) analyzeGame(Game.init, 1, scores, Opponent.init);
	}
}

string scoreFileName(Opponent opponent)
{
	return "data/scores-" ~ text(opponent) ~ ".f32";
}

void generateScoreFile(Opponent opponent)()
{
	import std.stdio : writeln;
	import std.file : write;

	writeln("=== Generating scores for ", opponent);

	auto fn = scoreFileName(opponent);
	Data tableData;
	ScoreTable table;
	if (fn.exists)
	{
		writeln("Resuming...");
		tableData = readData(fn); // not mapFile, we want to use this for scratch space
		table = cast(ScoreTable)tableData.contents;
	}
	else
	{
		tableData = Data(packedGameMax * Score.sizeof);
		table = cast(ScoreTable)tableData.contents;
		writeln("Initializing scores...");
		initializeScores(table);
	}

	auto nextTableData = Data(packedGameMax * Score.sizeof);
	auto nextTable = cast(ScoreTable)nextTableData.contents;
	// bool[ubyte[4]] sawCRC;
	for (size_t iter = 0; ; iter++)
	{
		if (iter && iter % 10 == 0)
		{
			writeln("Saving checkpoint...");
			atomicWrite(fn, table);
		}
		writeln("Iteration #", iter);
		propagateScores!opponent(table, nextTable);
		auto totalChange = zip(table, nextTable).map!(pair => abs(double(pair[0]) - pair[1])).sum;
		auto maxChange = zip(table, nextTable).map!(pair => abs(double(pair[0]) - pair[1])).reduce!max;
		auto numChanged = zip(table, nextTable).map!(pair => pair[0] == pair[1] ? 0L : 1L).sum;
		// auto crc = crc32Of(table);
		auto changeLimit = 1.0 / (2 ^^ (Score.mant_dig - 2)); // Calculate up to 22 bits of precision
		writeln(" > Change: num=", numChanged, " / ", packedGameMax,
			", total=", totalChange.fpToString,
			", avg=", (totalChange / table.length).fpToString,
			", max=", maxChange.fpToString, " / ", changeLimit.fpToString,
			// ", CRC=", crc[].format!"%(%02x%)",
		);
		swap(table, nextTable);
		if (maxChange <= changeLimit)
		// if (crc in sawCRC)
			break;
		// sawCRC[crc] = true;
	}
	writeln("Change threshold reached, saving...");
	atomicWrite(fn, table);
}

unittest
{
	if (false) generateScoreFile!(Opponent.init);
}

version (main_graph)
void main()
{
	static foreach (opponent; Opponent.init .. enumLength!Opponent)
		generateScoreFile!opponent;
}
