import std.algorithm.iteration;
import std.math.operations;

import common;

enum Opponent : ubyte
{
	perfect,  // Use graph
	ai1, // Ratau     - 10%
	ai2, // ?         - 30%
	ai3, // ?         - 50%
	ai4, // ?         - 80%
}

float[3] getAIMoveProbabilities(
	Game game,
	Die rolledDie,
	float difficulty, // out of 10
) @nogc
{
	float[3] result = 0;
	double chanceOfSmartChoice = difficulty / 10;
	double chanceOfStupidChoice = 1 - chanceOfSmartChoice;

	// "stupid" choice
	{
		size_t numViableColumns;
		foreach (column; 0 .. numColumns)
			if (game.board[Player.current][column].getNumDiceInColumn() < numRows)
				numViableColumns++;
		assert(numViableColumns > 0);
		foreach (column; 0 .. numColumns)
			if (game.board[Player.current][column].getNumDiceInColumn() < numRows)
				result[column] += chanceOfStupidChoice / numViableColumns;
	}

	// "smart" choice
	{
		int[3] columnScores;
		foreach (column; 0 .. numColumns)
		{
			if (game.board[Player.current][column].getNumDiceInColumn() >= numRows)
			{
				columnScores[column] = int.min;
				continue;
			}

			final switch (game.board[Player.other][column].getNumMatchesInColumn(rolledDie))
			{
				case 0:
					break;
				case 1:
					columnScores[column]++;
					break;
				case 2:
					columnScores[column] += 2;
					break;
				case 3:
					columnScores[column] += 5;
					break;
			}
			final switch (game.board[Player.current][column].getNumMatchesInColumn(rolledDie))
			{
				case 0:
					break;
				case 1:
					columnScores[column]++;
					break;
				case 2:
					columnScores[column] += 2;
					break;
				case 3:
					// columnScores[column] += 5;
					// break;
					assert(false);
			}
		}

		int bestScore = int.min;
		foreach (column; 0 .. numColumns)
			if (columnScores[column] > bestScore)
				bestScore = columnScores[column];

		size_t numViableColumns;
		foreach (column; 0 .. numColumns)
			if (columnScores[column] == bestScore)
				numViableColumns++;
		assert(numViableColumns > 0);
		foreach (column; 0 .. numColumns)
			if (columnScores[column] == bestScore)
				result[column] += chanceOfSmartChoice / numViableColumns;
	}

	assert(result[].sum.isClose(1f));
	return result;
}
