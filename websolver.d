import std.algorithm.comparison;
import std.algorithm.iteration;
import std.exception;
import std.file;
import std.stdio;
import std.string;

import ae.net.ssl.openssl;
import ae.sys.datamm;
import ae.utils.json;
import ae.utils.meta;

import ai;
import common;
import graph;
import packing;
import remotegraph;

mixin SSLUseLib;

struct Problem
{
	Opponent opponent;
	Game initialGame;
	Die rolledDie;
}

void main(string[] args)
{
	enforce(args.length == 2, "Bad usage");
	auto problem = args[1].jsonParse!Problem;

	// auto scoreData = mapFile(scoreFileName(problem.opponent), MmMode.read);
	// auto scores = cast(const(Score)[])scoreData.contents;

	auto scores = new RemoteScores(problem.opponent);
	Analysis analysis;
	do
		analysis = analyzeGame(problem.initialGame, problem.rolledDie, scores, problem.opponent);
	while (!scores.resolve());
	stdout.writeln(analysis.toJson.replace("nan", "null"));
}
