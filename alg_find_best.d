import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.mutation;
import std.conv;
import std.exception;
import std.file;
import std.parallelism;
import std.random;
import std.range;
import std.stdio;
import std.string;

import ae.sys.data;
import ae.sys.datamm;
// import ae.utils.array;
import ae.sys.file;
import ae.utils.json;
import ae.utils.meta;

import alg_common;
import alg_corpus_common : CorpusResult, opponent, scoreTable;

immutable GameUnpacking[] corpus;

shared static this()
{
	import core.runtime : Runtime;
	enforce(Runtime.args.length == 2, "Specify corpus");
	corpus = Runtime.args[1].readText.jsonParse!CorpusResult.games.idup;
}

struct EvaluationResult
{
	float score;
	float avgError;
}

EvaluationResult evaluateAlgorithm(ref const Algorithm algorithm)
{
	float score = 0;

	float avgError = 0;
	foreach (ref game; corpus)
		avgError += evaluateAlgorithmForGame!opponent(algorithm, game, scoreTable);
	avgError /= corpus.length;
	score += avgError;

	enum float complexityMalus = 1f/128;
	foreach (rule; algorithm.rules)
		if (rule != Rule.never)
			foreach (condition; rule.conditions)
				if (condition != Condition.always)
					score += (complexityMalus / numRules / numConditions);

	return EvaluationResult(
		score,
		avgError,
	);
}


void main()
{
	foreach (_; size_t.max.iota.parallel)
	{
		while (true)
		{
			RNG rng;
			auto seed = unpredictableSeed;
			rng.seed(seed);

			Algorithm bestAlgorithm = genAlgorithm(rng);
			auto bestScore = evaluateAlgorithm(bestAlgorithm);

			enum numAlgorithms = 48;
			enum stallLimit = 1024;

			size_t iteration;
			size_t lastImprovement = 0;
			while (lastImprovement + stallLimit > iteration)
			{
				iteration++;

				Algorithm[numAlgorithms] nextAlgorithms;
				foreach (i, ref algorithm; nextAlgorithms)
				{
					algorithm = bestAlgorithm;
					mutateAlgorithm(algorithm, rng);
				}

				EvaluationResult[numAlgorithms] nextScores;
				foreach (i; numAlgorithms.iota.parallel(1))
					nextScores[i] = evaluateAlgorithm(nextAlgorithms[i]);

				foreach (i; 0 .. numAlgorithms)
					if (nextScores[i].score < bestScore.score)
					{
						bestScore = nextScores[i];
						bestAlgorithm = nextAlgorithms[i];
						lastImprovement = iteration;
						writefln("%12s @ %8s: %s", seed, iteration, bestScore);

						auto fn = "algorithm-" ~ text(seed) ~ ".json";
						struct Result { EvaluationResult result; size_t iteration; Algorithm algorithm; }
						Result(bestScore, iteration, bestAlgorithm).toPrettyJson.atomic!writeTo(fn);
					}
			}
		}
	}
}
