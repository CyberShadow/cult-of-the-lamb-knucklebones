import std.algorithm.comparison;
import std.algorithm.iteration;
import std.conv;
import std.datetime.systime;
import std.file;
import std.parallelism;
import std.random;
import std.range;
import std.stdio;

import ae.sys.file;
import ae.utils.json;

import alg_common;
import alg_corpus_common : CorpusResult, opponent, scoreTable;

// ---------------------------------------------------------------------------------------

void main(string[] args)
{
	auto corpusFileNames = args[1..$];
	auto corpuses = corpusFileNames.map!(fn => fn.readText.jsonParse!CorpusResult).array;

	foreach (_; size_t.max.iota.parallel)
	{
		RNG rng;
		rng.seed(unpredictableSeed);

		real worstError = 0;

		while (true)
		{
			auto algorithm = genAlgorithm(rng);
			real maxAvg = -real.max; size_t maxAvgCorpus;
			real minAvg =  real.max; size_t minAvgCorpus;
			foreach (corpusIndex, corpus; corpuses)
			{
				real sum = 0;
				foreach (ref game; corpus.games)
					sum += evaluateAlgorithmForGame!opponent(algorithm, game, scoreTable);
				auto avg = sum / corpus.games.length;
				if (avg > maxAvg) { maxAvg = avg; maxAvgCorpus = corpusIndex; }
				if (avg < minAvg) { minAvg = avg; minAvgCorpus = corpusIndex; }
			}

			auto error = maxAvg - minAvg;
			if (error > worstError)
			{
				writeln(error);
				worstError = error;
				struct Result
				{
					real error;
					Algorithm algorithm;
					CorpusResult[2] corpuses;
				}
				Result(error, algorithm, [corpuses[minAvgCorpus], corpuses[maxAvgCorpus]])
					.toPrettyJson
					.toFile("algorithm-testcorpus-" ~ Clock.currTime.stdTime.to!string ~ ".json");
			}

			worstError *= 0.9999;
		}
	}
}
