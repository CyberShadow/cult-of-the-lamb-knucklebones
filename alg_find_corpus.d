import std.conv;
import std.parallelism;
import std.random;
import std.range;
import std.stdio;

import ae.sys.file;
import ae.utils.json;

import alg_corpus_common;

// ---------------------------------------------------------------------------------------

void main()
{
	IntegerScore[] checkpointHighScores;

	foreach (_; size_t.max.iota.parallel())
	{
		auto seed = unpredictableSeed;
		rndGen.seed(seed);

		auto bestCorpus = randomCorpus();
		auto bestScore = bestCorpus.getCorpusScore();

		enum stallLimit = 128*1024;
		enum checkpointInterval = 16*1024;
		enum checkpointThresholdMultiplier = 2;

		size_t iteration;

		alias StopReason = CorpusResult.StopReason;
		StopReason stopReason = StopReason.running;

		void save()
		{
			auto fn = "corpus-" ~ text(seed) ~ ".json";
			CorpusResult(stopReason, bestScore, numAlgorithms, iteration, bestCorpus.games).toPrettyJson.atomic!writeTo(fn);
		}

		size_t lastImprovement = 0;
		while (true)
		{
			iteration++;

			auto edit = randomEdit();
			auto newScore = tryEdit(bestCorpus, edit);
			if (newScore < bestScore)
			{
				writefln("%12s @ %8s : %15s - %s", seed, iteration, bestScore, edit.numEdits);
				applyEdit(bestCorpus, edit);
				assert(bestCorpus.getCorpusScore() == newScore);
				bestScore = newScore;
				lastImprovement = iteration;
			}

			if (iteration % checkpointInterval == 0)
			{
				synchronized
				{
					auto checkpointIndex = iteration / checkpointInterval;
					if (checkpointHighScores.length <= checkpointIndex)
						checkpointHighScores.length = checkpointIndex + 1;

					auto checkpointScore = checkpointHighScores[checkpointIndex];
					if (checkpointScore == 0) // first
						checkpointHighScores[checkpointIndex] = bestScore;
					else
					if (checkpointScore > bestScore) // new record
						checkpointHighScores[checkpointIndex] = bestScore;
					else
					if (checkpointScore * checkpointThresholdMultiplier < bestScore) // under threshold
					{
						writefln("%12s @ %8s : %15s - below checkpoint (%s) threshold (%s), abandoning",
							seed, iteration, bestScore, checkpointScore, checkpointScore * checkpointThresholdMultiplier);
						stopReason = StopReason.checkpoint;
						break;
					}
				}

				save();
			}

			if (iteration >= lastImprovement + stallLimit)
			{
				writefln("%12s @ %8s : %15s - stalled, stopping", seed, iteration, bestScore);
				stopReason = StopReason.stall;
				break;
			}
		}

		save();
	}
}
